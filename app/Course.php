<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Course
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course query()
 * @mixin \Eloquent
 */
class Course extends Model
{
    const PUBLISHED = 1;
    const PENDING   = 2;
    const REJECTED  = 3;

    public function pathAttachment(){
        return "/images/courses/" . $this->picture;
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    protected $withCount = ['review', 'student'];

    public function category(){
        return $this->belongsTo(Category::class)
            ->select('id', 'name');
    }

    public function goal(){
        return $this->hasMany(Goal::class)
            ->select('id', 'course_id', 'goal');
    }

    public function level(){
        return $this->belongsTo(Level::class)
            ->select('id', 'name');
    }

    public function review(){
        return $this->hasMany(Review::class)
            ->select('id',
                    'user_id',
                    'course_id',
                    'rating',
                    'comment',
                    'created_at');
    }

    public function requirement(){
        return $this->hasMany(Requirement::class)
            ->select('id', 'course_id', 'requirement');
    }

    public function student(){
        return $this->belongsToMany(Student::class);
    }

    public function teacher(){
        return $this->belongsTo(Teacher::class);
    }

    public function getRatingAttribute(){
        return $this->review()->avg('rating');
    }

    public function relatedCourses(){
        return Course::with('review')->whereCategoryId($this->category->id)
            ->where('id', '!=', $this->id)
            ->latest()
            ->limit(6)
            ->get();
    }

}
