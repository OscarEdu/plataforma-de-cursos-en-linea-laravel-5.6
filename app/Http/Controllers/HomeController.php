<?php

namespace App\Http\Controllers;

use App\Course;
use Illuminate\Http\Request;

class HomeController extends Controller
{


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $courses = Course::withCount(['student'])
            ->with('category', 'teacher', 'review')
            ->where('status', Course::PUBLISHED)
            ->latest()
            ->paginate(12);

        //dd($courses);

        return view('home', compact('courses'));
    }
}
