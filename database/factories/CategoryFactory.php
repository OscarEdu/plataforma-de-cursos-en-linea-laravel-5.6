<?php

use Faker\Generator as Faker;

$factory->define(App\Category::class, function (Faker $faker) {
    return [
        'name'          => $faker->randomElement(['PHP','JAVASCRIPT','JAVA','KOTLIN','XML', 'PYTHON', 'C#', 'COBOL', 'HTML', 'CSS']),
        'description'   => $faker->sentence
    ];
});
